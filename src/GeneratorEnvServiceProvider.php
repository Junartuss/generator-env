<?php

namespace Junartuss\GeneratorEnv;

use Illuminate\Support\ServiceProvider;

class GeneratorEnvServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->commands([
            GenerateEnv::class
        ]);
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
