<?php

namespace Junartuss\GeneratorEnv;

use Illuminate\Console\Command;

class GenerateEnv extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generate:env';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate your .env file';

    protected $contentFile = "";

    protected $ligne = "\r\n";

    protected $positiveChoice = ['oui', 'yes', 'o', 'y'];

    protected $name_file = '.env';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->header();

        if(file_exists($this->name_file)){
            $this->line('File .env exist...');
            $this->info('1 : Update .env');
            $this->info('2 : Regenerate .env');

            $choice = $this->ask('Choice ? [1/2]');

            if($choice == '1'){
                $this->headerModify();
            } elseif($choice == '2'){
                $this->generate();
                $this->info('Regenerate .env success');
            } else {
                $this->error('Incorrect choice');
            }

        } else {
            $this->generate();
        }

    }

    protected function header()
    {
        $this->line('-----------------------------------');
        $this->info('https://gitlab.com/Junartuss');
        $this->info('Generate .env');
        $this->line('-----------------------------------');
    }

    protected function generate()
    {
        $this->contentFile = "APP_NAME=Laravel" . $this->ligne;
        $this->contentFile .= "APP_ENV=local" . $this->ligne;
        $this->contentFile .= "APP_KEY=" . $this->ligne;
        $this->contentFile .= "APP_DEBUG=true" . $this->ligne;
        $this->contentFile .= "APP_URL=http://localhost" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= "LOG_CHANNEL=stack" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= "DB_CONNECTION=mysql" . $this->ligne;
        $this->contentFile .= "DB_HOST=127.0.0.1" . $this->ligne;
        $this->contentFile .= "DB_PORT=3306" . $this->ligne;
        $this->contentFile .= "DB_DATABASE=laravel" . $this->ligne;
        $this->contentFile .= "DB_USERNAME=root" . $this->ligne;
        $this->contentFile .= "DB_PASSWORD=" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= "BROADCAST_DRIVER=log" . $this->ligne;
        $this->contentFile .= "CACHE_DRIVER=file" . $this->ligne;
        $this->contentFile .= "QUEUE_CONNECTION=sync" . $this->ligne;
        $this->contentFile .= "SESSION_DRIVER=file" . $this->ligne;
        $this->contentFile .= "SESSION_LIFETIME=120" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= "REDIS_HOST=127.0.0.1" . $this->ligne;
        $this->contentFile .= "REDIS_PASSWORD=null" . $this->ligne;
        $this->contentFile .= "REDIS_PORT=6379" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= "MAIL_DRIVER=smtp" . $this->ligne;
        $this->contentFile .= "MAIL_HOST=smtp.mailtrap.io" . $this->ligne;
        $this->contentFile .= "MAIL_PORT=2525" . $this->ligne;
        $this->contentFile .= "MAIL_USERNAME=null" . $this->ligne;
        $this->contentFile .= "MAIL_PASSWORD=null" . $this->ligne;
        $this->contentFile .= "MAIL_ENCRYPTION=null" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= "AWS_ACCESS_KEY_ID=" . $this->ligne;
        $this->contentFile .= "AWS_SECRET_ACCESS_KEY=" . $this->ligne;
        $this->contentFile .= "AWS_DEFAULT_REGION=us-east-1" . $this->ligne;
        $this->contentFile .= "AWS_BUCKET=" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= "PUSHER_APP_ID=" . $this->ligne;
        $this->contentFile .= "PUSHER_APP_KEY=" . $this->ligne;
        $this->contentFile .= "PUSHER_APP_SECRET=" . $this->ligne;
        $this->contentFile .= "PUSHER_APP_CLUSTER=mt1" . $this->ligne;

        $this->contentFile .= $this->ligne;

        $this->contentFile .= 'MIX_PUSHER_APP_KEY="${PUSHER_APP_KEY}"' . $this->ligne;
        $this->contentFile .= 'MIX_PUSHER_APP_CLUSTER="${PUSHER_APP_CLUSTER}"' . $this->ligne;

        $file = fopen($this->name_file, 'w');
        fwrite($file, $this->contentFile);

        $this->info(".env generated !");
        $value = $this->ask("Update .env ? [y/N]");
        if(in_array(strtolower($value), $this->positiveChoice)){
            $this->headerModify();
        }
    }

    protected function headerModify()
    {
        $this->info('Choose section ?');
        $this->info('1 . APP');
        $this->info('2 . DATABASE');
        $this->info('3 . REDIS');
        $this->info('4 . MAIL');
        $this->info('5 . AWS');
        $this->info('6 . PUSHER');
        $this->info('7 . Others modifications');

        $value = $this->ask('Choice ? [1/2/3/4/5/6/7]');
        $this->modify($value);
    }

    protected function modify($value)
    {
        $file = file_get_contents($this->name_file);
        $datas = explode($this->ligne, $file);

        switch ($value){
            case '1':
                $compteur = 0;
                foreach ($datas as $data){
                    if(!empty($data)){
                        if(strpos($data, 'APP_') !== false && strpos($data, '_APP_') == false){
                            $subData = explode('=', $data);
                            $value = $this->ask($subData[0] . " ? [" . $subData[1] . "]");
                            if(!empty($value)){
                                $subData[1] = $value;
                            }
                            $datas[$compteur] = implode('=', $subData);
                        }
                    }
                    $compteur++;
                }
                $newDatas = implode($this->ligne, $datas);
                break;
            case '2':
                $compteur = 0;
                foreach ($datas as $data){
                    if(!empty($data)){
                        if(strpos($data, 'DB_') !== false){
                            $subData = explode('=', $data);
                            $value = $this->ask($subData[0] . " ? [" . $subData[1] . "]");
                            if(!empty($value)){
                                $subData[1] = $value;
                            }
                            $datas[$compteur] = implode('=', $subData);
                        }
                    }
                    $compteur++;
                }
                $newDatas = implode($this->ligne, $datas);
                break;
            case '3':
                $compteur = 0;
                foreach ($datas as $data){
                    if(!empty($data)){
                        if(strpos($data, 'REDIS_') !== false){
                            $subData = explode('=', $data);
                            $value = $this->ask($subData[0] . " ? [" . $subData[1] . "]");
                            if(!empty($value)){
                                $subData[1] = $value;
                            }
                            $datas[$compteur] = implode('=', $subData);
                        }
                    }
                    $compteur++;
                }
                $newDatas = implode($this->ligne, $datas);
                break;
            case '4':
                $compteur = 0;
                foreach ($datas as $data){
                    if(!empty($data)){
                        if(strpos($data, 'MAIL_') !== false){
                            $subData = explode('=', $data);
                            $value = $this->ask($subData[0] . " ? [" . $subData[1] . "]");
                            if(!empty($value)){
                                $subData[1] = $value;
                            }
                            $datas[$compteur] = implode('=', $subData);
                        }
                    }
                    $compteur++;
                }
                $newDatas = implode($this->ligne, $datas);
                break;
            case '5':
                $compteur = 0;
                foreach ($datas as $data){
                    if(!empty($data)){
                        if(strpos($data, 'AWS_') !== false){
                            $subData = explode('=', $data);
                            $value = $this->ask($subData[0] . " ? [" . $subData[1] . "]");
                            if(!empty($value)){
                                $subData[1] = $value;
                            }
                            $datas[$compteur] = implode('=', $subData);
                        }
                    }
                    $compteur++;
                }
                $newDatas = implode($this->ligne, $datas);
                break;
            case '6':
                $compteur = 0;
                foreach ($datas as $data){
                    if(!empty($data)){
                        if(strpos($data, 'PUSHER_') !== false){
                            $subData = explode('=', $data);
                            $value = $this->ask($subData[0] . " ? (value:" . $subData[1] . ")");
                            if(!empty($value)){
                                $subData[1] = $value;
                            }
                            $datas[$compteur] = implode('=', $subData);
                        }
                    }
                    $compteur++;
                }
                $newDatas = implode($this->ligne, $datas);
                break;
            case '7':
                $compteur = 0;
                foreach ($datas as $data){
                    if(!empty($data)){
                        if(strpos($data, 'LOG_') !== false
                            || strpos($data, 'BROADCAST_') !== false
                            || strpos($data, 'CACHE_') !== false
                            || strpos($data, 'QUEUE_') !== false
                            || strpos($data, 'SESSION_') !== false){
                            $subData = explode('=', $data);
                            $value = $this->ask($subData[0] . " ? [" . $subData[1] . "]");
                            if(!empty($value)){
                                $subData[1] = $value;
                            }
                            $datas[$compteur] = implode('=', $subData);
                        }
                    }
                    $compteur++;
                }
                $newDatas = implode($this->ligne, $datas);
                break;
            default:
                $this->error('Incorrect choice');
                break;
        }

        file_put_contents($this->name_file, $newDatas);

        $this->line('----------------------------------------');
        $this->info('Update .env success');
        $value = $this->ask('Others modifications ? [y/N]');
        $this->line('----------------------------------------');

        if(in_array(strtolower($value), $this->positiveChoice)){
            $this->line('');
            $this->line('');
            $this->line('');
            $this->line('');
            $this->headerModify();
        }
    }
}
